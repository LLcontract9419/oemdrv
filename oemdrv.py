#!/usr/bin/env python

from __future__ import print_function

import sys
import tempfile
import shlex
import os
import subprocess

from jinja2 import Template

TEMPLATE = 'kickstart.cfg.j2'

GATEWAY_5 = '10.49.5.1'
GATEWAY_55 = '10.49.55.1'


try:
    d = {'hostname': sys.argv[1],
         'ipaddress': sys.argv[2]}
except IndexError:
    print('%s <hostname> <ipaddress> [nodocker]' % (sys.argv[0]), file=sys.stderr)
    sys.exit(1)

try:
    if sys.argv[3] == 'nodocker':
        d['docker'] = False
except IndexError:
    d['docker'] = True



if d['ipaddress'].startswith('10.49.'):
    #
    # VicRoads Contract 9419
    #

    d['domain'] = 'itss.roads.vic.gov.au'
    d['netmask'] = '255.255.255.0'
    d['ntpservers'] = ['10.49.5.13', '10.49.5.6', '10.49.5.136', '10.49.55.136']

    if d['ipaddress'].startswith('10.49.5.'):
        d['gateway'] = GATEWAY_5
        d['nameserver1'] = '10.49.5.132'
        d['nameserver2'] = '10.49.55.132'
    elif d['ipaddress'].startswith('10.49.55.'):
        d['gateway'] = GATEWAY_55
        d['nameserver1'] = '10.49.55.132'
        d['nameserver2'] = '10.49.5.132'
    else:
        print('Invalid IP Address!')
        sys.exit(1)

elif d['ipaddress'].startswith('10.1.'):
    #
    # Home 10.1.1.0
    #
    d['domain'] = 'juenemann.local'
    d['netmask'] = '255.255.255.0'
    d['ntpservers'] = ['0.au.pool.ntp.org', '1.au.pool.ntp.org', '2.au.pool.ntp.org']
    d['nameserver1'] = '10.1.1.101'
    d['nameserver2'] = '10.1.1.1'
    d['gateway'] = '10.1.1.1'

else:
    print('Invalid IP Address!')
    sys.exit(1)


with open(TEMPLATE) as fp:
    template = Template(fp.read())

kickstart = template.render(**d)

tmpdir = tempfile.mkdtemp()

with open(os.path.join(tmpdir, 'ks.cfg'), 'w') as fp:
    fp.write(kickstart)

cmd = shlex.split('mkisofs -verbose -volid "OEMDRV" -o %s.iso %s' % (d['hostname'], tmpdir))
proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
stdout,_ = proc.communicate()
print(stdout)
print('** ENSURE TO GIVE THE VM AT LEAST 40 GB DISK SPACE ***')
